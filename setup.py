#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name="stele1contrib-bonus",
    version="1.0.0",
    author="Daniel M",
    author_email="dan.mntg@gmail.com",
    description="stele1, bonus package",
    url="https://codeberg.org/stele-climbing/stele1contrib-bonus",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX",
        "Development Status :: 3 - Alpha",
    ],
    python_requires='>=3.6',
)
