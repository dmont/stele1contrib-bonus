# import stele1.photo

from hashlib import sha256
from PIL import ExifTags, Image
from shapely.geometry import Point, box

class Blob:
    def __init__(self, path):
        self.path = path
        self.img = Image.open(self.path)

    def get_sha256(self):
        h = sha256()
        with open(self.path, "rb") as f:
            for b in iter(lambda: f.read(4096), b""):
                 h.update(b)
        return h.hexdigest()

    def minimal_resize(self, size, orientation=None):
        """
        implementation based of the PIL.ImageOps.exif_transpose:
        https://github.com/python-pillow/Pillow/blob/7.0.0/src/PIL/ImageOps.py#L527-L551
        """
        image = self.img.copy()
        exif = image.getexif()
        if orientation is None:
            orientation = exif.get(0x0112)
        method = {
            2: Image.FLIP_LEFT_RIGHT,
            3: Image.ROTATE_180,
            4: Image.FLIP_TOP_BOTTOM,
            5: Image.TRANSPOSE,
            6: Image.ROTATE_270,
            7: Image.TRANSVERSE,
            8: Image.ROTATE_90,
        }.get(orientation)
        if method is not None:
            transposed_image = image.transpose(method)
            # del exif[0x0112]
            # transposed_image.info["exif"] = exif.tobytes()
            transposed_image.thumbnail(size, Image.ANTIALIAS)
            return transposed_image
        else:
            image.thumbnail(size, Image.ANTIALIAS)
            return image

    def get_exif(self, *args):
        tags = {}
        for tag, value in self.img.getexif().items():
            tagname = ExifTags.TAGS.get(tag)
            if tagname == "GPSInfo":
                gpstags = {}
                for gpstag in value:
                    gpstagname = ExifTags.GPSTAGS.get(gpstag)
                    gpstags[gpstagname] = value[gpstag]
                    gpstags[gpstagname] = value[gpstag]
                tags[tagname] = gpstags
            else:
                tags[tagname] = value
        cursor = tags
        for arg in args:
            if cursor != None:
                cursor = cursor.get(arg, None)
            else:
                return None
        return cursor

    def get_exif_fields(self):
      f = {}
      exif = self.img.getexif()
      for code, name in ExifTags.TAGS.items():
        if name == 'GPSInfo':
          for k, v in exif.get_ifd(code).items():
            f[ExifTags.GPSTAGS.get(k, k)] = v
        elif name == "ExifOffset":
          for k, v in exif.get_ifd(code).items():
            f[ExifTags.TAGS.get(k, k)] = v
      for k, v in exif.items():
        f[ExifTags.TAGS.get(k, k)] = v
      return f


    def get_longitude(self):
        dms = self.get_exif_fields().get("GPSLongitude")
        ref = self.get_exif_fields().get("GPSLongitudeRef")
        if dms and ref:
            d, m, s = dms
            lat = d + (m / 60.0) + (s / 3600.0)
            if "W" == ref:
                lat *= -1
            return lat
        else:
            return None

    def get_latitude(self):
        dms = self.get_exif_fields().get("GPSLatitude")
        ref = self.get_exif_fields().get("GPSLatitudeRef")
        if dms and ref:
            d, m, s = dms
            lat = d + (m / 60.0) + (s / 3600.0)
            if "S" == ref:
                lat *= -1
            return lat
        else:
            return None

    def get_exif_latlng(self):
        lat = self.get_latitude()
        lng = self.get_longitude()
        if lat and lng:
            return {'latitude': lat, 'longitude': lng}
        else:
            return None

    def get_exif_datetimeoriginal(self):
        return self.get_exif_fields().get("DateTimeOriginal")

    def get_exif_orientation(self):
        return self.get_exif_fields().get("Orientation")

    @staticmethod
    def from_store(store, uuid):
        imgpath = store.get_photo_image_by_uuid(uuid)
        if imgpath:
            return Blob(imgpath)

def location_point(store, uuid):
    p = store.get_photo_by_uuid(uuid)
    l = p.get_real_location()
    if l:
        return Point(l.to_data()['longitude'], l.to_data()['latitude'])
    imgpath = store.get_photo_image_by_uuid(uuid)
    if not imgpath:
        return None
    img = Blob(imgpath)
    exif_coords = img.get_exif_latlng()
    if exif_coords:
        return Point(exif_coords['longitude'], exif_coords['latitude'])
    else:
        return None

# def location_polygon(store, uuid):
# def location_box(store, uuid):
