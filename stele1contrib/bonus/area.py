# import stele1.area

from shapely.geometry import Point, Polygon 

arbitrary_tiny_degree = 0.00681025505066 - 0.00676733970644

def location_polygon(store, uuid):
    def from_circle(circle):
        lon = circle['longitude']
        lat = circle['latitude']
        rad = circle['meterRadius']
        print('FIXME: buffer is not based on the radius (X4oqYs)')
        return Point(lon, lat).buffer(arbitrary_tiny_degree)
    def from_polygon(pts):
        pairs = []
        for pt in pts:
            pairs.append((pt['longitude'], pt['latitude']))
        return Polygon(pairs)
    p = store.get_area_by_uuid(uuid)
    l = p.get_location()
    if l and l.to_data()['type'] == 'approximation': 
        return from_circle(l.to_data()['circle'])
    elif l and l.to_data()['type'] == 'perimeter': 
        return from_polygon(l.to_data()['polygon'])
    else:
        return None
