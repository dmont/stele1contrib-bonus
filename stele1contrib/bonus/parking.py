# import stele1.parking

from shapely.geometry import Point

def location_point(store, uuid):
    p = store.get_parking_by_uuid(uuid)
    l = p.get_location()
    if l:
        return Point(l.to_data()['longitude'], l.to_data()['latitude'])
    else:
        return None
