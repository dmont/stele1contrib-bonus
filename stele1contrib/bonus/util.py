from math import sin, cos, radians

# see https://en.wikipedia.org/wiki/Geographic_coordinate_system
# and https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
def meters_per_degree_lat(lat):
    a = radians(lat)
    return 111132.954 - (559.822 * cos(2 * a)) + (1.175 * cos(4 * a))
def meters_per_degree_lon(lat):
    return 111132.954 * cos(radians(lat))
def approximate_meters_to_degrees(meters, at_latitude=None):
    mpd_lat = meters_per_degree_lat(at_latitude)
    mpd_lon = meters_per_degree_lon(at_latitude)
    mpd_avg = (mpd_lat + mpd_lon) / 2
    dpm_avg = 1 / mpd_avg
    return dpm_avg * meters

def rise(deg):
    return cos(radians(deg))

def run(deg):
    return sin(radians(deg))

def difference(a, b):
    return max(a, b) - min(a, b)

def interpolate(range, posn):
    start, end = range
    return posn * (end - start) + start

def normalize(range, posn):
    start, end = range
    return (posn - start) / (end - start) 

class LinearRange:
    def __init__(self, domain, range):
        self.domain = domain
        self.range = range
    def convert(self, pt):
        return interpolate(self.range, normalize(self.domain, pt))

class Bounds:
    def __init__(self, point1, point2):
        x1, y1 = point1
        x2, y2 = point2
        self._xmin = min(x1, x2)
        self._xmax = max(x1, x2)
        self._ymin = min(y1, y2)
        self._ymax = max(y1, y2)
    def __repr__(self):
        return f'Bounds(({self._xmin},{self._ymin}),({self._xmax},{self._ymax}))'
    def __str__(self):
        return f'(({self._xmin},{self._ymin}),({self._xmax},{self._ymax}))'
    @property
    def xmin(self):
        return self._xmin
    @property
    def xmax(self):
        return self._xmax
    @property
    def ymax(self):
        return self._ymax
    @property
    def ymin(self):
        return self._ymin
    @property
    def height(self):
        return self._ymax - self._ymin
    @property
    def width(self):
        return self._xmax - self._xmin
    def copy(self):
        return Bounds((self.xmin, self.ymin), (self.xmax, self.ymax))
    def extend(self, x, y):
        self._xmin = min(self._xmin, x)
        self._xmax = max(self._xmax, x)
        self._ymin = min(self._ymin, y)
        self._ymax = max(self._ymax, y)
    def set_height_gravity_center(self, h):
        vcenter = self.ymax - (self.height / 2)
        self._ymin = vcenter - (h / 2)
        self._ymax = vcenter + (h / 2)
    def set_width_gravity_center(self, w):
        hcenter = self.xmax - (self.width / 2)
        self._xmin = hcenter - (w / 2)
        self._xmax = hcenter + (w / 2)
    def pad_to_aspect_ratio(self, ratio):
        self_ratio = self.width / self.height
        if self_ratio < ratio:
            self.set_width_gravity_center(self.height / ratio)
        elif self_ratio > ratio:
            self.set_height_gravity_center(self.width * (1 / ratio))
