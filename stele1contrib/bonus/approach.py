# import stele1.approach

from shapely.geometry import LineString

def location_polygon(store, uuid):
    raise NotImplementedError('it might be implemented... I have no idea if this is correct')
    p = store.get_area_by_uuid(uuid)
    l = p.get_location()
    if l: 
        pairs = []
        for pt in l.to_data():
            pairs.append((pt['longitude'], pt['latitude']))
        return LineString(pairs)
    else:
        return None
